module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/ad/'
      : '/',
      transpileDependencies: [
        'vue-echarts',
        'resize-detector'
      ]
  }