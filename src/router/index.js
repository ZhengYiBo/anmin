import Vue from 'vue'
import VueRouter from 'vue-router'
import Loong from '../views/index.vue'
import shou from '../views/login.vue'
import users from '../views/users.vue'
import roles from '../views/rights/roles'
import ridsid from '../views/rights/ridsid'
import goods from '../views/goods/goods'
import goodschil from '../views/goods/godchild'
import goodsquan from '../views/goods/goodsquan'
import goodsfen from '../views/goods/goodsfen'
import reports from '../views/reports'
import load from '../views/load'
import store from '../store'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    redirect:'/Loong'
  },
  {
    path:'/Loong',
    name:'Loong',
    component:Loong,
    meta:{
      notAuthorization:true
    }
  },
  {
    path:'/shou',
    name:'shou',
    component:shou,
    children:[
      {
        path:'/shou',
        redirect:'users'
      },
      {
        path:'users',
        name:'users',
        component:users
      },
      {
        path:'roles',
        name:'roles',
        component:roles,
      },
      {
        path:'rights',
        name:'ridsid',
        component:ridsid,
      },
      {
        path:'goods',
        name:'goods',
        component:goods,
      },
      {
        path:'goodschil',
        name:'goodschil',
        component:goodschil,
      },
      {
        path:'biagoods/:id',
        name:'biagoods',
        component:goodschil,
        props:true
      },
      {
        path:'params',
        name:'goodsquan',
        component:goodsquan,
      },
      {
        path:'categories',
        name:'goodsfen',
        component:goodsfen,
      },
      {
        path:'reports',
        name:'reports',
        component:reports,
      },
      {
        path:'orders',
        name:'load',
        component:load,
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to,from,next)=>{
  if(!to.meta.notAuthorization){
    if(store.state.token){
      next();
    }else{
      next({
        path:'/Loong'
      })
    }
  }else{
    next();
  }
})

export default router
