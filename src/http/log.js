import {http} from './index';
import {Message} from 'element-ui';
export function gettoken() {
    return http('menus','GET').then(res=>{
        return res.data;
    })
}
export function getlei(params) {
    return http('users','GET',{},params).then(res=>{
        return res.data;
    })
}

export function handerAdd(data){
    return http('users','POST',data).then(res=>{
        Message({
            type:'success',
            message:'添加成功'
        })
        return res.data;
    })
}

// 更新用户状态
export function xiugai(uid,type){
    return http(`users/${uid}/state/${type}`,'PUT').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 编辑用户
export function bianji(obj){
    return http(`users/${obj.id}`,'PUT',{
        mobile:obj.mobile,
        email:obj.email
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 删除用户
export function deled(id){
    return http(`users/${id}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 用户身份
export function shenfenyan(id){
    return http(`users/${id}`,'GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 获取角色列表
export function getusersss(){
    return http('roles','GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 分配用户角色
export function tijiaojuese(id,rid){
    return http(`users/${id}/role`,'PUT',{
        id,
        rid
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 添加角色
export function justjia(data){
    return http('roles','POST',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 获取角色列表
export function liejusbi(){
    return http('roles','GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 编辑角色提交
export function bjianss(data){
    return http(`roles/${data.id}`,'PUT',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 删除角色权限
export function remnovejuse(roleId,rightId){
    return http(`roles/${roleId}/rights/${rightId}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 获取所有权限列表
export function suoyouqx(){
    return http('rights/tree','GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 商品列表分类
export function goodsfei(params){
    return http('goods','GET',{},params).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 获取商品分类列表
export function goodslist(){
    return http('categories','GET',{},{
        type:3
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}


// 获取商品参数
export function getgoodlist(id,sel){
    return http(`categories/${id}/attributes`,'GET',{},{
        sel
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}


// 添加商品参数
export function addgoodsd(data){
    return http('goods','POST',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}


// 根据id查询商品
export function cahxunshag(id){
    return http(`goods/${id}`,'GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}


// 添加修改商品信息
export function xiugaigoods(data){
    return http(`goods/${data.goods_id}`,'PUT',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 编辑提交参数222
export function updataload(id,attrId,data){
    return http(`categories/${id}/attributes/${attrId}`,'PUT',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}



// 商品分类列表
export function goodsfeiddd(params){
    return http('categories','GET',{},params,{type:3}).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}



// 数据统计折线图
export function zhexiantu(){
    return http('reports/type/1','GET').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}


// 订单数据列表
export function getolder(params){
    return http('orders','GET',{},params).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}

// 删除参数
export function deeldasd(id,attrid){
    return http(`categories/${id}/attributes/${attrid}`,'DELETE').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data;
    })
}