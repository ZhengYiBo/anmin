import axios from 'axios';
import { Message } from 'element-ui'
import store from '../store/index'
import router from '../router'
axios.defaults.baseURL = "http://120.26.128.6:8889/api/private/v1/"

axios.interceptors.response.use(function (response) {
    if(response.data.meta.status===400){
        router.push({
            name:'Loong'
        })
    }
    console.log('拦截器',response)
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

export const http = (url, method = "get", data,params) => {
    return new Promise((resolve, reject) => {
        axios({
            url,
            method,
            headers: {
                authorization: store.state.token,
            },
            data,
            params,
        }).then(res => {
            if (res.status >= 200 && res.status < 300 || res.status == 304) {
                if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
                    resolve(res.data);
                } else {
                    Message.error(res.data.meta.msg)
                    reject(res);
                }
            } else {
                Message.error(res.statusText)
                reject(res);
            }
        }).catch(err => {
            reject(err)
        })
    })
}