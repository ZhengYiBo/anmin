import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function getstat(){
  return {
    token:localStorage.getItem('token')||'',
    username:localStorage.getItem('username')||'',
  }
}

export default new Vuex.Store({
  state: getstat(),
  mutations: {
    settoken(state,data){
      state.token=data;
      localStorage.setItem('token',data)
    },
    setusername(state,data){
      state.username=data;
      localStorage.setItem('username',data)
    },
    install(state,data){
      localStorage.clear();
      Object.assign(state,getstat());
    }
  },
  actions: {
  },
  modules: {
  }
})
