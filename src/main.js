import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/index.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import {http} from '@/http'
import dayjs from 'dayjs'
import ECharts from 'vue-echarts'

Vue.component('v-chart', ECharts)
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.prototype.$http=http

Vue.filter('dealdate',(val)=>{
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
